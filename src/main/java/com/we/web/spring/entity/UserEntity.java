/**
 * UserEntity.java
 * com.we.web.spring.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.we.core.db.entity.BaseEntity;

/**
 * 用户实体
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月6日
 */
@Data
@Table(name = "as_user")
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends BaseEntity {
	@Column
	private String name;
}
