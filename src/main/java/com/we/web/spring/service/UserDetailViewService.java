/**
 * UserDetailViewService.java
 * com.we.web.spring.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.we.core.db.dao.SqlMapper;
import com.we.core.db.page.Page;
import com.we.core.db.util.SqlUtil;
import com.we.web.spring.form.userdetail.UserDetaiDetailForm;
import com.we.web.spring.form.userdetail.UserDetailListForm;
import com.we.web.spring.form.userdetail.UserDetailUpdateForm;

/**
 * 用户详情视图服务
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年7月20日
 */
@Service
public class UserDetailViewService {

	@Autowired
	private SqlMapper sqlMapper;

	/**
	 * 填充列表信息
	 *
	 * @param userDetailListForm 用户详情列表表单
	 * @param page 分页
	 * @param model 模型
	*/
	public void setListInfo(final UserDetailListForm userDetailListForm, final Page page, final Model model) {
		model.addAttribute("list", SqlUtil.list(sqlMapper, userDetailListForm, page));
	}

	/**
	 * 填充详情信息
	 *
	 * @param userDetaiDetailForm 用户详情详情表单
	 * @param model
	*/
	public void setDetailInfo(final UserDetaiDetailForm userDetaiDetailForm, final Model model) {
		model.addAttribute("userDetail", SqlUtil.get(sqlMapper, userDetaiDetailForm));
	}

	/**
	 * 更新
	 *
	 * @param userDetailUpdateForm 用户详情更新表单
	*/
	public void update(final UserDetailUpdateForm userDetailUpdateForm) {

	}

}
