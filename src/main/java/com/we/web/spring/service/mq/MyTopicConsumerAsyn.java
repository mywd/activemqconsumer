/**
 * MyTopicConsumerAsyn.java
 * com.we.web.spring.service.mq
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.service.mq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;

import com.we.web.spring.entity.UserEntity;
import com.we.web.spring.service.UserBaseService;

/**
 * 异步主题消费者
 *
 * @author   MiaoYang(147621629@qq.com)
 * @Date	 2017年8月2日 	 
 */
public class MyTopicConsumerAsyn implements MessageListener {
	@Autowired
	private UserBaseService userBaseService;

	@Override
	public void onMessage(Message message) {
		try {
			TextMessage msg = (TextMessage) message;
			UserEntity userEntity = new UserEntity();
			userEntity.setName(msg.getText() + "topic1");
			userBaseService.add(userEntity);
			System.out.println("Topic1消费了MyTopic中的消息=======================" + msg.getText());
		} catch (JMSException e) {
			e.printStackTrace();

		}
	}

}
