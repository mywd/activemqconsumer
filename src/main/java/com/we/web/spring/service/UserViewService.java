/**
 * UserViewService.java
 * com.we.web.spring.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.we.core.db.dao.SqlMapper;
import com.we.core.db.page.Page;
import com.we.core.db.util.SqlUtil;
import com.we.web.spring.entity.UserEntity;
import com.we.web.spring.form.user.UserAddForm;
import com.we.web.spring.form.user.UserListForm;
import com.we.web.spring.form.user.UserUpdateForm;

/**
 * 用户视图业务
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年7月24日
 */
@Service
public class UserViewService {
	@Autowired
	private SqlMapper sqlMapper;
	@Autowired
	private UserBaseService userBaseService;

	/**
	 * 填充列表信息
	 *
	 * @param userListForm 用户列表表单
	 * @param page 分页
	 * @param model 模型
	*/
	public void setListInfo(final UserListForm userListForm, final Page page, final Model model) {
		model.addAttribute("list", SqlUtil.list(sqlMapper, userListForm, page, UserEntity.class));
	}

	/**
	 * 填充详情信息
	 *
	 * @param id id
	 * @param model 模型
	*/
	public void setDetailInfo(final long id, final Model model) {
		model.addAttribute("user", userBaseService.get(id));
	}

	/**
	 * 填充添加信息
	 *
	 * @param model 模型
	*/
	public void setAddInfo(final Model model) {
	}

	/**
	 * 添加
	 *
	 * @param userAddForm 用户添加表单
	 * @return 用户对象
	*/
	public UserEntity add(final UserAddForm userAddForm) {
		return userBaseService.add(userAddForm.toEntity());
	}

	/**
	 * 填充更新信息
	 *
	 * @param id id
	 * @param model 模型
	*/
	public void setUpdateInfo(final long id, final Model model) {
		model.addAttribute("user", userBaseService.get(id));
	}

	/**
	 * 更新
	 *
	 * @param userUpdateForm 用户更新表单
	*/
	public void update(final UserUpdateForm userUpdateForm) {
		userBaseService.update(userUpdateForm.toEntity());
	}

	/**
	 * 删除
	 *
	 * @param id id
	*/
	public void delete(final long id) {
		userBaseService.delete(id);
	}

}
