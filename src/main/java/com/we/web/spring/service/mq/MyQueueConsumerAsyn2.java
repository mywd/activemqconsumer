/**
 * MyQueueConsumer.java
 * com.we.web.spring.service.mq
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.service.mq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * 队列消息消费者异步的方式
 *
 * @author   MiaoYang(147621629@qq.com)
 * @Date	 2017年8月2日 	 
 */
public class MyQueueConsumerAsyn2 implements MessageListener {

	@Override
	public void onMessage(Message message) {
		TextMessage msg = (TextMessage) message;
		try {
			System.out.println("Queue2消费了MyQueue中消息================异步监听的方式===================" + msg.getText());
		} catch (JMSException e) {
			e.printStackTrace();

		}
	}
}
