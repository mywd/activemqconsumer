/**
 * UserEditForm.java
 * com.we.web.spring.form
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.form.user;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

import com.we.core.db.form.ITransferEntity;
import com.we.web.spring.entity.UserEntity;

/**
 * 用户添加表单
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月18日
 */
@Data
public class UserAddForm implements ITransferEntity<UserEntity> {

	/**
	 * 名称
	 */
	@NotBlank
	private String name;
}
