/**
 * UserDetailListForm.java
 * com.we.web.spring.form.userdetail
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.form.userdetail;

import lombok.Data;

import com.we.core.db.form.ISqlForm;

/**
 * 用户详情表单
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年7月6日
 */
@Data
public class UserDetaiDetailForm implements ISqlForm {
	/**
	 * id
	 */
	private long id;

	@Override
	public String getSqlKey() {
		return "com.we.web.spring.web.UserDetailController.detail";
	}

}
