/**
 * UserController.java
 * com.we.web.spring.web
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.we.core.db.page.Page;
import com.we.web.spring.form.user.UserAddForm;
import com.we.web.spring.form.user.UserListForm;
import com.we.web.spring.form.user.UserUpdateForm;
import com.we.web.spring.service.UserViewService;

/**
 * 用户控制器
 * <p>
 * 适用于单对象操作
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月6日
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserViewService userViewService;

	/**
	 * 列表
	 * 
	 * @param userListForm 用户列表表单
	 * @param page 分页
	 * @param model 模型
	 */
	@RequestMapping
	@ModelAttribute
	public void list(@ModelAttribute final UserListForm userListForm, @ModelAttribute final Page page, final Model model) {
		userViewService.setListInfo(userListForm, page, model);
	}

	/**
	 * 详情
	 * 
	 * @param id id
	 * @param model 模型
	 */
	@RequestMapping
	public void detail(final long id, final Model model) {
		userViewService.setDetailInfo(id, model);
	}

	/**
	 * 新增
	 */
	@GetMapping
	public void add(final Model model) {
		userViewService.setAddInfo(model);
	}

	/**
	 * 新增
	 * 
	 * @param userAddForm 用户新增表单
	 */
	@PostMapping
	@ResponseBody
	public Object add(final UserAddForm userAddForm) throws Exception {
		return userViewService.add(userAddForm);
	}

	/**
	 * 更新
	 * 
	 * @param id id
	 * @param model 模型
	 */
	@GetMapping
	public void update(final long id, final Model model) {
		userViewService.setUpdateInfo(id, model);
	}

	/**
	 * 更新
	 * 
	 * @param userUpdateForm 用户更新表单
	 * 
	 */
	@PostMapping
	@ResponseBody
	public void update(final UserUpdateForm userUpdateForm) throws Exception {
		userViewService.update(userUpdateForm);
	}

	/**
	 * 删除
	 * 
	 * @param id id
	 */
	@ResponseBody
	@RequestMapping
	public void delete(final long id) {
		userViewService.delete(id);
	}
}
